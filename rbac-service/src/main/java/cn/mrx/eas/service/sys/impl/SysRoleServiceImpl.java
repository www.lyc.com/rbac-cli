package cn.mrx.eas.service.sys.impl;

import cn.mrx.eas.dao.sys.SysPermissionMapper;
import cn.mrx.eas.dao.sys.SysRoleMapper;
import cn.mrx.eas.dao.sys.SysRolePermissionMapper;
import cn.mrx.eas.dao.sys.SysUserRoleMapper;
import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.dto.sys.SysRole;
import cn.mrx.eas.exception.EasException;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.service.sys.ISysRoleService;
import cn.mrx.eas.vo.sys.TreeNode;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 上午11:42
 * Description:
 */
@Slf4j
@Service
public class SysRoleServiceImpl implements ISysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public BSGrid roleListPage(Integer curPage, Integer pageSize) {
        Page<SysRole> sysRolePage = PageHelper.startPage(curPage, pageSize);
        List<SysRole> sysRoleList = sysRoleMapper.roleListPage();
        return new BSGrid(true, curPage, pageSize, sysRolePage.getTotal(), null, null, sysRoleList);
    }

    @Override
    public SysRole findOneById(Integer id) {
        SysRole sysRole = sysRoleMapper.findOneById(id);
        if (sysRole == null) {
            throw new EasException(404, "没有该条数据");
        }
        return sysRole;
    }

    @Override
    public List<TreeNode> allNodes(Integer roleId) {
        // 所有权限
        List<SysPermission> sysPermissionList = sysPermissionMapper.findAll();
        // 该角色所拥有的权限
        List<SysPermission> havePermissionList = sysPermissionMapper.havePermissionList(roleId);
        // 构建权限树集合
        List<TreeNode> treeNodeList = new ArrayList<>();
        for (SysPermission sysPermission : sysPermissionList) {
            TreeNode treeNode = new TreeNode();
            treeNode.setId(sysPermission.getId());
            treeNode.setParentId(sysPermission.getParentId());
            treeNode.setName(sysPermission.getName());
            // 选中状态
            for (SysPermission havePermission : havePermissionList) {
                if (sysPermission.getId() == havePermission.getId()) {
                    treeNode.setChecked(true);
                }
            }
            treeNodeList.add(treeNode);
        }
        return treeNodeList;
    }

    @Transactional
    @Override
    public ServerResponse editRole(SysRole sysRole, String permissionIds) {
        // 修改角色基本信息
        int result = sysRoleMapper.editRole(sysRole);
        // 删除该角色所拥有的权限
        sysRolePermissionMapper.deletePermissionByRoleId(sysRole.getId());
        // 为该角色赋予新的权限(这里加一个判断,因为可能一个权限也不赋予)
        if (StringUtils.isNotBlank(permissionIds)) {
            String[] permissionIdArr = permissionIds.split(",");
            sysRolePermissionMapper.assignPermission(sysRole.getId(), permissionIdArr);
        }
        if (result > 0) return ServerResponse.success("修改成功");
        return ServerResponse.error("修改失败");
    }

    @Transactional
    @Override
    public ServerResponse addRole(SysRole sysRole, String permissionIds) {
        // 添加角色基本信息
        int result = sysRoleMapper.addRole(sysRole);
        // 为该角色赋予权限(这里加一个判断,因为可能一个权限也不赋予)
        if (StringUtils.isNotBlank(permissionIds)) {
            String[] permissionIdArr = permissionIds.split(",");
            sysRolePermissionMapper.assignPermission(sysRole.getId(), permissionIdArr); // 这里的id是插入成功后返回的主键id
        }
        if (result > 0) return ServerResponse.success("添加成功");
        return ServerResponse.error("添加失败");
    }

    @Transactional
    @Override
    public ServerResponse batchDel(String ids) {
        String[] idArr = ids.split(",");
        // 系统默认角色禁止删除
        for (String id : idArr) {
            if (id.equals("1") || id.equals("2") || id.equals("3")) {
                return ServerResponse.error("系统默认角色禁止删除");
            }
        }
        // 删除角色
        int result = sysRoleMapper.batchDel(idArr);
        // 把该角色对应的中间表数据也删除掉
        for (String id : idArr) {
            sysRolePermissionMapper.deletePermissionByRoleId(Integer.valueOf(id));
            sysUserRoleMapper.deleteRoleByRoleId(Integer.valueOf(id));
        }
        if (result > 0) return ServerResponse.success("批量操作成功");
        return ServerResponse.error("批量操作失败");
    }

    @Override
    public List<SysRole> findAllByUserId(Integer userId) {
        List<SysRole> sysRoleList = sysRoleMapper.findAll();
        List<SysRole> selfSysRoleList = sysRoleMapper.findAllByUserId(userId);
        for (SysRole sysRole : sysRoleList) {
            for (SysRole selfSysRole : selfSysRoleList) {
                if (sysRole.getId() == selfSysRole.getId()) {
                    sysRole.setIsSelf(true);
                }
            }
        }
        return sysRoleList;
    }
}