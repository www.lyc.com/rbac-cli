package cn.mrx.eas.dto.sys;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class SysRolePermission implements Serializable {

    private static final long serialVersionUID = 806991306525705549L;
    private Integer id;
    private Integer roleId;
    private Integer permissionId;

    // ######################## Constructor&ToString ########################
    public SysRolePermission() {
    }

    public SysRolePermission(Integer id, Integer roleId, Integer permissionId) {
        this.id = id;
        this.roleId = roleId;
        this.permissionId = permissionId;
    }

    @Override
    public String toString() {
        return "SysRolePermission{" +
                "id=" + id +
                ", roleId=" + roleId +
                ", permissionId=" + permissionId +
                '}';
    }
}