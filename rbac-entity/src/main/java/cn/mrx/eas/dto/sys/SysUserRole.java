package cn.mrx.eas.dto.sys;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = -4119577762633389888L;
    private Integer id;
    private Integer userId;
    private Integer roleId;

    // ######################## Constructor&ToString ########################
    public SysUserRole() {
    }

    public SysUserRole(Integer id, Integer userId, Integer roleId) {
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
                "id=" + id +
                ", userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }
}