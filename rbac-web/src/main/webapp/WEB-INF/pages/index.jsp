﻿<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>RBAC-CLI - 权限脚手架系统</title>
    <meta name="keywords" content="RBAC,CLI,权限,权限脚手架">
    <meta name="description" content="一款基于Spring+SpringMVC+Mybatis+Shiro的一个权限脚手架，用户可在此基础上很方便的进行二次开发！">
    <!--浏览器默认内核指定-->
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <!--指定IE8浏览器去模拟某个特定版本的IE浏览器的渲染方式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--设置移动端自适应-->
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <!--禁止搜索引擎转码-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <!--在浏览器收藏夹中显示的图标-->
    <link rel="Bookmark" href="/resources/favicon.ico">
    <!--网页标题小图标-->
    <link rel="Shortcut Icon" href="/resources/favicon.ico"/>
    <!--css引入-->
    <link rel="stylesheet" type="text/css" href="/resources/static/h-ui/css/H-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/static/h-ui.admin/css/H-ui.admin.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/lib/Hui-iconfont/1.0.8/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/static/h-ui.admin/skin/default/skin.css" id="skin"/>
    <link rel="stylesheet" type="text/css" href="/resources/static/h-ui.admin/css/style.css"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/resources/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/resources/lib/respond.min.js"></script>
    <![endif]-->
    <!--[if IE 6]>
    <script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
</head>
<body>
<header class="navbar-wrapper">
    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"> <a class="logo navbar-logo f-l mr-10 hidden-xs" href="/">EAS</a> <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/">EAS</a>
            <span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav class="nav navbar-nav">
                <ul class="cl">
                    <li class="dropDown dropDown_hover"><a href="javascript:;" class="dropDown_A"><i class="Hui-iconfont">&#xe600;</i> 新增 <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onclick="article_add('添加资讯','article-add.html')"><i class="Hui-iconfont">&#xe616;</i> 资讯</a></li>
                            <li><a href="javascript:;" onclick="picture_add('添加资讯','picture-add.html')"><i class="Hui-iconfont">&#xe613;</i> 图片</a></li>
                            <li><a href="javascript:;" onclick="product_add('添加资讯','product-add.html')"><i class="Hui-iconfont">&#xe620;</i> 产品</a></li>
                            <li><a href="javascript:;" onclick="member_add('添加用户','member-add.html','','510')"><i class="Hui-iconfont">&#xe60d;</i> 用户</a></li>
                        </ul>
                    </li>
                    <li class="dropDown dropDown_hover">
                        <a href="javascript:;" class="dropDown_A">工具 <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li>
                                <a href="" target="_blank">Bug兼容性汇总</a>
                            </li>
                            <li>
                                <a href="" target="_blank">web安全色</a>
                            </li>
                            <li>
                                <a href="" target="_blank">Hui-iconfont</a>
                            </li>
                            <li>
                                <a href="javascript:;">web工具箱<i class="arrow Hui-iconfont">&#xe6d7;</i></a>
                                <ul class="menu">
                                    <li>
                                        <a href="" target="_blank">JS/HTML格式化工具</a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">HTML/JS转换工具</a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">CSS代码格式化工具</a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">字母大小写转换工具</a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">繁体字、火星文转换</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">三级菜单<i class="arrow Hui-iconfont">&#xe6d7;</i></a>
                                        <ul class="menu">
                                            <li>
                                                <a href="javascript:;">四级菜单</a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">四级菜单</a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">四级菜单</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">三级导航</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">二级导航</a>
                            </li>
                            <li class="disabled">
                                <a href="javascript:;">二级菜单</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">
                    <li>欢迎</li>
                    <li class="dropDown dropDown_hover">
                        <a href="#" class="dropDown_A"><shiro:principal property="username"/> <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onClick="pi()">个人信息</a></li>
                            <li><a href="#">切换账户</a></li>
                            <li><a href="/logout">退出</a></li>
                        </ul>
                    </li>
                    <li id="Hui-msg"> <a href="#" title="消息"><span class="badge badge-danger">1</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a> </li>
                    <li id="Hui-skin" class="dropDown right dropDown_hover"> <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<aside class="Hui-aside">
    <div class="menu_dropdown bk_2">
        <shiro:hasPermission name="ROLE_PERMISSION_MANAGER">
        <dl id="menu-admin">
            <dt><i class="Hui-iconfont">&#xe62d;</i> 角色权限管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="/sys/role" data-title="角色管理" href="javascript:void(0)">角色管理</a></li>
                    <li><a data-href="/sys/permission" data-title="权限管理" href="javascript:void(0)">权限管理</a></li>
                    <li><a data-href="/sys/user" data-title="用户管理" href="javascript:void(0)">用户管理</a></li>
                </ul>
            </dd>
        </dl>
        </shiro:hasPermission>

        <shiro:hasPermission name="SYSTEM_MANAGER">
        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 系统管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="/druid" data-title="数据源监控" href="javascript:void(0)">数据源监控</a></li>
                    <li><a data-href="/404Test" data-title="404测试" href="javascript:void(0)">404测试</a></li>
                    <li><a data-href="/500Test" data-title="500测试" href="javascript:void(0)">500测试</a></li>
                </ul>
            </dd>
        </dl>
        </shiro:hasPermission>

        <shiro:hasPermission name="STRESS_RELIEVER">
        <dl id="menu-jianya">
            <dt><i class="Hui-iconfont">&#xe68e;</i> 程序员减压<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="/stress1" data-title="山区" href="javascript:void(0)">山区</a></li>
                    <li><a data-href="/stress2" data-title="烤火" href="javascript:void(0)">烤火</a></li>
                    <li><a data-href="/stress3" data-title="沙滩" href="javascript:void(0)">沙滩</a></li>
                    <li><a data-href="/stress4" data-title="下雨声" href="javascript:void(0)">下雨声</a></li>
                </ul>
            </dd>
        </dl>
        </shiro:hasPermission>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<section class="Hui-article-box">
    <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
        <div class="Hui-tabNav-wp">
            <ul id="min_title_list" class="acrossTab cl">
                <li class="active">
                    <span title="我的桌面" data-href="welcome.html">我的桌面</span><em></em>
                </li>
            </ul>
        </div>
        <div class="Hui-tabNav-more btn-group">
            <a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a>
            <a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
        </div>
    </div>
    <div id="iframe_box" class="Hui-article">
        <div class="show_iframe">
            <div style="display:none" class="loading"></div>
            <iframe scrolling="yes" frameborder="0" src="/welcome"></iframe>
        </div>
    </div>
</section>

<div class="contextMenu" id="Huiadminmenu">
    <ul>
        <li id="closethis">关闭当前</li>
        <li id="closeall">关闭全部</li>
    </ul>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/resources/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/resources/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/resources/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/resources/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script>
<script type="text/javascript">
    $(function () {
        /*$("#min_title_list li").contextMenu('Huiadminmenu', {
            bindings: {
                'closethis': function(t) {
                    console.log(t);
                    if(t.find("i")){
                        t.find("i").trigger("click");
                    }
                },
                'closeall': function(t) {
                    alert('Trigger was '+t.id+'\nAction was Email');
                },
            }
        });*/
    });

    // 个人信息(Personal information)
    function pi() {
        var userId = "<shiro:principal property='id'/>";
        layer.open({
            title: '个人信息',
            type: 2,
            shade: false,
            maxmin: true,
            shade: 0.5,
            area: ['80%', '80%'],
            content: '/sys/user/pi/' + userId,
            zIndex: layer.zIndex,
            end: function () {
                gridObj.refreshPage();
            }
        });
    }

    /*资讯-添加*/
    function article_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }

    /*图片-添加*/
    function picture_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }

    /*产品-添加*/
    function product_add(title, url) {
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }

    /*用户-添加*/
    function member_add(title, url, w, h) {
        layer_show(title, url, w, h);
    }
</script>
</body>
</html>